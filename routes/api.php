<?php

use App\Http\Controllers\CompanyController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\EmployeeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//generi routes
Route::resource('company', CompanyController::class);
Route::resource('employee', EmployeeController::class);

//special routes
Route::get('generate/pdf/{company}', [DocumentController::class, 'generateEmployeePdfBasedOnCompanyId']);

//Example of what inside resource routes
// 1. Index Route
// Route::get('/company', 'CompanyController@index')->name('company.index');
// // 2. Create Route
// Route::get('/company/create', 'CompanyController@create')->name('company.create');
// // 3. Store Route
// Route::post('/company', 'CompanyController@store')->name('company.store');
// // 4. Show Route
// Route::get('/company/{company}', 'CompanyController@show')->name('company.show');
// // 5. Edit Route
// Route::get('/company/{company}/edit', 'CompanyController@edit')->name('company.edit');
// // 6. Update Route
// Route::put('/company/{company}', 'CompanyController@update')->name('company.update');
// // 7. Destroy Route
// Route::delete('/company/{company}', 'CompanyController@destroy')->name('company.destroy');


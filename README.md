# [Advance Leetcode] HR Management

This HR Management Apps, mostly use for me to demo on how can i use REST APIs, laravel snappy with wkhtmltopdf, and login with laravel/ui vite package.

## References

* Laravel UI:[ https://github.com/laravel/ui](https://github.com/laravel/ui)
* Laravel Snappy: [https://github.com/KnpLabs/snappy#wkhtmltopdf-binary-as-composer-dependencies](https://github.com/KnpLabs/snappy#wkhtmltopdf-binary-as-composer-dependencies)
* Wkhtmltopdf: [https://wkhtmltopdf.org/](https://wkhtmltopdf.org/)

## Properties

* Programming Lang: PHP 8.2.3
* Framework: Laravel 9.52.16
* Database: MySQL 5.7.33+
* Init Tools: Laragon
* Javascript Tools: JQuery 3.7.1
* REST APIs Test Tool: [Insomnia REST](https://insomnia.rest/)

## Installation

1. Clone Repository
2. Run "composer install"
3. Run "npm install"
4. Create .env file, copy from env.example
5. Adjust where wkhtmltopdf and wkhtmltoimg location are on .env
   1. If it in Debian, you can empty it or if it not work, you need to install it, follow guide from website
   2. If it in Windows, you can install it from [https://wkhtmltopdf.org/](https://wkhtmltopdf.org/), and set .exe path from it
6. Adjust database on .env, and connect it
7. Run "php artisan migrate"
8. Run "php artisan db:seed"
9. Run "php artisan serve" to serve back-end (normal port is 8000)
10. Run "npm run dev" to serve the front-end part
11. Visit http://localhost:8000/login
12. Login according to seeder on UsersTableSeeder.php on database/seeders

## APIs Testing

* You need to install [Insomnia REST](https://insomnia.rest/)
* Then you can register or use local env
* Create collections and enter the collections
* Import insomnia_api.json from [documentations](documentations)
* Use private ENV (you can change base_url to whatever url you want, normal url is http://localhost:8000)

## Developer Note

* For APIs testing, pls adjust port / url according to your need, or use only http://localhost:8000

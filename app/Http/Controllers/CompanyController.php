<?php

namespace App\Http\Controllers;

use App\Services\CompanyService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{

    private $companyService;
    public function __construct(CompanyService $companyService){
        $this->companyService = $companyService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $result = $this->companyService->get();
            return response()->json(['data' => $result, 'status' => 'Success', 'message' => ""], 200);
        } catch (\Throwable $th) {
            Log::error('Exception caught:', [
                'class' => get_class($th),
                'message' => $th->getMessage(),
                'file' => $th->getFile(),
                'line' => $th->getLine(),
                'stack_trace' => $th->getTraceAsString(),
            ]);
            return response()->json(['data' => null, 'status' => 'failed', "message" => $th->getMessage()], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validatedRequest = array_filter($request->validate([
                'name' => 'required|string',
                'logo_link' => 'required|image|mimes:png|max:2048|dimensions:min_width=100,min_height=100',
                'email' => 'required|string'
            ]));

            $result = $this->companyService->create($validatedRequest);
            return response()->json(['data' => $result, 'status' => 'Success', 'message' => ""], 200);
        } catch (\Throwable $th) {
            Log::error('Exception caught:', [
                'class' => get_class($th),
                'message' => $th->getMessage(),
                'file' => $th->getFile(),
                'line' => $th->getLine(),
                'stack_trace' => $th->getTraceAsString(),
            ]);
            return response()->json(['data' => null, 'status' => 'failed', "message" => $th->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {

            $result = $this->companyService->get([
                "where" => ['id' => $id]
            ]);

            return response()->json(['data' => $result, 'status' => 'Success', 'message' => ""], 200);
        } catch (\Throwable $th) {
            Log::error('Exception caught:', [
                'class' => get_class($th),
                'message' => $th->getMessage(),
                'file' => $th->getFile(),
                'line' => $th->getLine(),
                'stack_trace' => $th->getTraceAsString(),
            ]);
            return response()->json(['data' => null, 'status' => 'failed', "message" => $th->getMessage()], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $validatedRequest = array_filter($request->validate([
                'name' => 'nullable|string',
                'logo_link' => 'nullable|image|mimes:png|max:2048|dimensions:min_width=100,min_height=100',
                'email' => 'nullable|string'
            ]));

            $validatedRequest['id'] = $id;

            $result = $this->companyService->update($validatedRequest);
            return response()->json(['data' => $result, 'status' => 'Success', 'message' => ""], 200);
        } catch (\Throwable $th) {
            Log::error('Exception caught:', [
                'class' => get_class($th),
                'message' => $th->getMessage(),
                'file' => $th->getFile(),
                'line' => $th->getLine(),
                'stack_trace' => $th->getTraceAsString(),
            ]);
            return response()->json(['data' => null, 'status' => 'failed', "message" => $th->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $validatedRequest = Validator::make(['id' => $id], [
                'id' => 'required|numeric',
            ])->validate();

            $result = $this->companyService->delete($validatedRequest);
            return response()->json(['data' => $result, 'status' => 'Success', 'message' => ""], 200);
        } catch (\Throwable $th) {
            Log::error('Exception caught:', [
                'class' => get_class($th),
                'message' => $th->getMessage(),
                'file' => $th->getFile(),
                'line' => $th->getLine(),
                'stack_trace' => $th->getTraceAsString(),
            ]);
            return response()->json(['data' => null, 'status' => 'failed', "message" => $th->getMessage()], 500);
        }
    }
}

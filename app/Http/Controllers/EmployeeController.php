<?php

namespace App\Http\Controllers;

use App\Services\EmployeeService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{

    private $employeeService;
    public function __construct(EmployeeService $employeeService){
        $this->employeeService = $employeeService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $result = $this->employeeService->get();
            return response()->json(['data' => $result, 'status' => 'Success', 'message' => ""], 200);
        } catch (\Throwable $th) {
            Log::error('Exception caught:', [
                'class' => get_class($th),
                'message' => $th->getMessage(),
                'file' => $th->getFile(),
                'line' => $th->getLine(),
                'stack_trace' => $th->getTraceAsString(),
            ]);
            return response()->json(['data' => null, 'status' => 'failed', "message" => $th->getMessage()], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validatedRequest = array_filter($request->validate([
                'name' => 'required|string',
                //check primary key
                'company_id' => 'required|numeric|exists:companies,id',
                'email' => 'required|string'
            ]));

            $result = $this->employeeService->create($validatedRequest);
            return response()->json(['data' => $result, 'status' => 'Success', 'message' => ""], 200);
        } catch (\Throwable $th) {
            Log::error('Exception caught:', [
                'class' => get_class($th),
                'message' => $th->getMessage(),
                'file' => $th->getFile(),
                'line' => $th->getLine(),
                'stack_trace' => $th->getTraceAsString(),
            ]);
            return response()->json(['data' => null, 'status' => 'failed', "message" => $th->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {

            $result = $this->employeeService->get([
                "where" => ['id' => $id]
            ]);

            return response()->json(['data' => $result, 'status' => 'Success', 'message' => ""], 200);
        } catch (\Throwable $th) {
            Log::error('Exception caught:', [
                'class' => get_class($th),
                'message' => $th->getMessage(),
                'file' => $th->getFile(),
                'line' => $th->getLine(),
                'stack_trace' => $th->getTraceAsString(),
            ]);
            return response()->json(['data' => null, 'status' => 'failed', "message" => $th->getMessage()], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $validatedRequest = array_filter($request->validate([
                'name' => 'required|string',
                //check primary key
                'company_id' => 'required|numeric|exists:companies,id',
                'email' => 'required|string'
            ]));

            $validatedRequest['id'] = $id;

            $result = $this->employeeService->update($validatedRequest);
            return response()->json(['data' => $result, 'status' => 'Success', 'message' => ""], 200);
        } catch (\Throwable $th) {
            Log::error('Exception caught:', [
                'class' => get_class($th),
                'message' => $th->getMessage(),
                'file' => $th->getFile(),
                'line' => $th->getLine(),
                'stack_trace' => $th->getTraceAsString(),
            ]);
            return response()->json(['data' => null, 'status' => 'failed', "message" => $th->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $validatedRequest = Validator::make(['id' => $id], [
                'id' => 'required|numeric',
            ])->validate();

            $result = $this->employeeService->delete($validatedRequest);
            return response()->json(['data' => $result, 'status' => 'Success', 'message' => ""], 200);
        } catch (\Throwable $th) {
            Log::error('Exception caught:', [
                'class' => get_class($th),
                'message' => $th->getMessage(),
                'file' => $th->getFile(),
                'line' => $th->getLine(),
                'stack_trace' => $th->getTraceAsString(),
            ]);
            return response()->json(['data' => null, 'status' => 'failed', "message" => $th->getMessage()], 500);
        }
    }
}

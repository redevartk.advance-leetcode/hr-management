<?php

namespace App\Http\Controllers;

use App\Services\DocumentService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class DocumentController extends Controller
{
    private $documentService;

    public function __construct(DocumentService $documentService){
        $this->documentService = $documentService;
    }

    public function generateEmployeePdfBasedOnCompanyId($id){
        try {
            $validatedRequest = Validator::make(['companyId' => $id], [
                'companyId' => 'required|numeric',
            ])->validate();

            $result = $this->documentService->generateEmployeePdfBasedOnCompanyId($validatedRequest);
            return $result;
        } catch (\Throwable $th) {
            Log::error('Exception caught:', [
                'class' => get_class($th),
                'message' => $th->getMessage(),
                'file' => $th->getFile(),
                'line' => $th->getLine(),
                'stack_trace' => $th->getTraceAsString(),
            ]);
            return response()->json(['data' => null, 'status' => 'failed', "message" => $th->getMessage()], 500);
        }
    }


}

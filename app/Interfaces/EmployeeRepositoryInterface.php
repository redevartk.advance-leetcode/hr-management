<?php

namespace App\Interfaces;

use App\Models\Employee;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface EmployeeRepositoryInterface
{
    /**
     * Get employees based on specified conditions.
     *
     * @param array|null $data
     * @return LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function get($data = null);

    /**
     * Create a new employee.
     *
     * @param array $data
     * @return Employee
     */
    public function create(array $data);

    /**
     * Update an employee based on specified conditions.
     *
     * @param array $data
     * @return Employee|null
     */
    public function update(array $data);

    /**
     * Delete an employee based on specified conditions.
     *
     * @param array $data
     * @return bool|null
     */
    public function delete(array $data);
}

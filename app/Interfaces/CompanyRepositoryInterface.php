<?php

namespace App\Interfaces;

use App\Models\Company;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface CompanyRepositoryInterface
{
    /**
     * Get companies based on specified conditions.
     *
     * @param array|null $data
     * @return LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function get($data = null);

    /**
     * Create a new company.
     *
     * @param array $data
     * @return Company
     */
    public function create(array $data);

    /**
     * Update a company based on specified conditions.
     *
     * @param array $data
     * @return Company|null
     */
    public function update(array $data);

    /**
     * Delete a company based on specified conditions.
     *
     * @param array $data
     * @return bool|null
     */
    public function delete(array $data);
}

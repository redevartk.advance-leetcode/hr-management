<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;

class FileService
{

    private $saveFolderName = "company";
    public function uploadResumeAndGetPath($data){
        $file = $data['logo_link'];

        if ($file->isValid()) {
            $randomNumber = rand(0, 999999);//fast solutions for now,

            $fileName = $randomNumber."-".str_replace(' ', '-', $file->getClientOriginalName());

            // Save the file to the default storage (usually 'storage/app/public' directory)
            $storeFile = $file->storeAs($this->saveFolderName, $fileName);

            $buildFilePath = $this->saveFolderName."/".str_replace(' ', '-', $fileName);

            return $buildFilePath;
        }

        return 'null';
    }

    public function deleteFileLocalPath($data){
        $filePath = $data['link'];

        // Check if the file exists
        if (Storage::exists($filePath)) {

            // Delete the file
            Storage::delete($filePath);

            return 'null';
        }

        return 'null';
    }
}

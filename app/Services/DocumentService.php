<?php

namespace App\Services;

use App\Repositories\EmployeeRepository;
use Barryvdh\Snappy\Facades\SnappyPdf;

class DocumentService
{
    private $SnappyPdf;
    private $employeeRepository;

    public function __construct(SnappyPdf $SnappyPdf, EmployeeRepository $employeeRepository)
    {
        $this->SnappyPdf = $SnappyPdf;
        $this->employeeRepository = $employeeRepository;
    }

    public function generateEmployeePdfBasedOnCompanyId($data){
        $employeeData = $this->employeeRepository->get([
            "where" => ['company_id' => $data['companyId']]
        ]);

        $formattedData = [];

        foreach ($employeeData as $employee) {
            // Assuming $employee has 'name' and 'email' properties
            $formattedData[] = [
                'name' => $employee->name,
                'email' => $employee->email,
            ];
        }

        $formattedData = mb_convert_encoding($formattedData, "UTF-8", "auto");

        $pdf = SnappyPdf::loadView('documents.employee-pdf', ['data' => $formattedData]);

        return $pdf->download('employee-data.pdf');
    }
}

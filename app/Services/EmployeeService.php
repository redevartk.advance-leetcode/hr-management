<?php

namespace App\Services;

use App\Repositories\EmployeeRepository;

class EmployeeService
{
    private $employeeRepository;

    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    public function get($data = null){
        $init = $this->employeeRepository->get($data);
        return $init;
    }

    public function update($data){
        $where = $data['id'];

        $init = $this->employeeRepository->update([
            "where" => ["id" => $where],
            "updateData" => $data
        ]);

        return $init;
    }

    public function create($data){
        $init = $this->employeeRepository->create(["data" => $data]);
        return $init;
    }

    public function delete($data){
        $getData = $this->employeeRepository->get([
            "where" => ['id' => $data['id']]
        ]);

        if(empty($getData[0])){
            throw new \Exception("there's no data with Id: ".$data['id']);
        } else {
            $getData = $getData[0];
        }

        $init = $this->employeeRepository->delete([
            'where' => ['id' => $data['id']]
        ]);

        return [];
    }
}

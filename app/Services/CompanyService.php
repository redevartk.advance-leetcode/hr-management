<?php

namespace App\Services;

use App\Repositories\CompanyRepository;

class CompanyService
{
    private $companyRepository, $fileService;

    public function __construct(CompanyRepository $companyRepository, FileService $fileService)
    {
        $this->companyRepository = $companyRepository;
        $this->fileService = $fileService;
    }

    public function get($data = null){
        $init = $this->companyRepository->get($data);
        return $init;
    }

    public function update($data){
        $where = $data['id'];

        //upload file if new is exist
        if(!empty($data['logo_link'])){
            $getData = $this->companyRepository->get([
                "where" => ['email' => $data['email']]
            ])[0];

            if(!empty($getData['logo_link'])){
                $this->fileService->deleteFileLocalPath([
                    'link' => $getData['logo_link']
                ]);
            }

            $data['logo_link'] = $this->fileService->uploadResumeAndGetPath($data);
        }

        $init = $this->companyRepository->update([
            "where" => ["id" => $where],
            "updateData" => $data
        ]);

        return $init;
    }

    public function create($data){
        if(!empty($data['logo_link'])){
            $data['logo_link'] = $this->fileService->uploadResumeAndGetPath($data);
        }

        $init = $this->companyRepository->create(["data" => $data]);
        return $init;
    }

    public function delete($data){
        $getData = $this->companyRepository->get([
            "where" => ['id' => $data['id']]
        ]);

        if(empty($getData[0])){
            throw new \Exception("there's no data with Id: ".$data['id']);
        } else {
            $getData = $getData[0];
        }

        $this->fileService->deleteFileLocalPath([
            'link' => $getData['logo_link']
        ]);

        $init = $this->companyRepository->delete([
            'where' => ['id' => $data['id']]
        ]);

        return [];
    }
}

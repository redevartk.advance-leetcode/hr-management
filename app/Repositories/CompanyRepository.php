<?php

namespace App\Repositories;

use App\Interfaces\CompanyRepositoryInterface;
use App\Models\Company;

class CompanyRepository implements CompanyRepositoryInterface{

    private $model;

    public function __construct(Company $model){
        $this->model = $model;
    }

    public function get($data = null){

        $init = $this->model;

        if(!empty($data['where'])){
            foreach($data['where'] as $whereKey => $whereVal){
                $init = $init->where($whereKey, $whereVal);
            }
        }

        if(!empty($data) && !empty($data['pagination'])){
            $result = $init->paginate($data['pagination']);
        } else {
            $result = $init->get();
        }

        return $result;
    }

    public function create($data){
        $init = $this->model;

        $init = $init->create($data['data']);

        return $init;
    }

    public function update($data){
        $init = $this->model;

        if (!empty($data['where'])) {
            foreach ($data['where'] as $whereKey => $whereVal) {
                $init = $init->where($whereKey, $whereVal);
            }
        }

        //get the data
        $modelInstance = $init->first();

        if (!empty($modelInstance)) {
            $modelInstance->update($data['updateData']);

            return $modelInstance;
        }

        // Return null if not found
        return null;
    }

    public function delete($data){
        $init = $this->model;

        if(!empty($data['where'])){
            foreach($data['where'] as $whereKey => $whereVal){
                $init = $init->where($whereKey, $whereVal);
            }
        }

        return $init->delete();
    }
}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
    <hr>
    <br>

    <!-- Company Table -->
    <div class="container mt-4">
        <h2>Company Table</h2>
        <table class="table" id="company-table">
            <thead class="table-dark">
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Logo</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
                {{-- javascript filled --}}
            </tbody>
        </table>
    </div>

    <!-- Employee Table -->
    <div class="container mt-4">
        <h2>Employee Table</h2>
        <table class="table" id="employee-table">
            <thead class="table-dark">
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                {{-- javascript filled --}}
            </tbody>
        </table>
    </div>

    <script>
        $(document).ready(function () {
            // Fetch data on page load
            fetchCompanyData();
            fetchEmployeeData();

            // Print PDF button click event
            $(document).on('click', '.print-pdf-btn', function () {
                 // Get the company ID from the data-company-id attribute
                const companyId = $(this).data('company-id');
                const apiUrl = `{{ url('/api/generate/pdf/${companyId}') }}`;

                $.ajax({
                    url: apiUrl,
                    method: 'GET',
                    success: function (data) {
                        // Process and populate data in the table
                        populateTable(data['data']);
                    },
                    error: function (error) {
                        console.error('Error fetching data:', error);
                    }
                });
            });

            // Function to fetch data from the backend
            function fetchCompanyData() {
                // Replace with your Laravel backend API URL
                const apiUrl = "{{ url('/api/company') }}";

                $.ajax({
                    url: apiUrl,
                    method: 'GET',
                    success: function (data) {
                        // Process and populate data in the table
                        populateCompanyTable(data['data']);
                    },
                    error: function (error) {
                        console.error('Error fetching data:', error);
                    }
                });
            }

            // Function to populate data in the table
            function populateCompanyTable(data) {
                const tableBody = $('#company-table tbody');
                tableBody.empty(); // Clear existing data

                // Loop through the data and append rows to the table
                $.each(data, function (index, company) {
                    const row = `
                        <tr>
                            <td>${company.name}</td>
                            <td>${company.email}</td>
                            <td><img src="storage/${company.logo_link}" alt="Company Logo" class="img-thumbnail" style="max-width: 50px;"></td>
                            <td>
                                <a href="/api/generate/pdf/${company.id}" class="btn btn-info btn-sm print-pdf-btn">Print PDF</a>
                            </td>
                        </tr>
                    `;

                    tableBody.append(row);
                });
            }

            // Function to fetch data from the backend
            function fetchEmployeeData() {
                // Replace with your Laravel backend API URL
                const apiUrl = "{{ url('/api/employee') }}";

                $.ajax({
                    url: apiUrl,
                    method: 'GET',
                    success: function (data) {
                        // Process and populate data in the table
                        populateEmployeeTable(data['data']);
                    },
                    error: function (error) {
                        console.error('Error fetching data:', error);
                    }
                });
            }

            // Function to populate data in the table
            function populateEmployeeTable(data) {
                const tableBody = $('#employee-table tbody');
                tableBody.empty(); // Clear existing data

                // Loop through the data and append rows to the table
                $.each(data, function (index, company) {
                    const row = `
                        <tr>
                            <td>${company.name}</td>
                            <td>${company.email}</td>
                        </tr>
                    `;

                    tableBody.append(row);
                });
            }
        });
    </script>
</div>

@endsection
